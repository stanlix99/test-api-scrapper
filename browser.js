const puppeteer = require('puppeteer');
function run () {
    return new Promise(async (resolve, reject) => {
        try {
            const browser = await puppeteer.launch();
            const page = await browser.newPage();
            await page.goto("https://www.toornament.com/es/games/valorant");
            let urls = await page.evaluate(() => {
                let results = [];
                let items = document.querySelectorAll('div.tournaments-list-open');
                items.forEach((item) => { 
                   results.push({
                        text: item.innerText,
                        url: item.getAttribute('href'),
                        name: item.getAttribute('name'),
                        status: item.getAttribute('status')
                    });
                });
                return results;
            })
            browser.close();
            return resolve(urls);
        } catch (e) {
            return reject(e);
        }
    })
    
}


run().then(console.log).catch(console.error);




function download_txt() {
  var textToSave = results;
  var hiddenElement = document.createElement('a');

  hiddenElement.href = 'data:attachment/text,' + encodeURI(textToSave);
  hiddenElement.target = '_blank';
  hiddenElement.download = 'myFile.json';
  hiddenElement.click();
}




function runInfo(){
    return new Promise(async (resolve, reject) => {
        try {
            const browser = await puppeteer.launch();
            const page = await browser.newPage();
            await page.goto("https://www.toornament.com/es/games/valorant");
            let urls = await page.evaluate(() => {
                let results = [];
                let items = document.querySelectorAll('a.tournament-linked');
                items.forEach((item) => {
                    results.push({
                        text: item.innerText,
                        url: item.getAttribute('href')
                    });
                });
                return results;
            })
            browser.close();
            return resolve(urls);
        } catch (e) {
            return reject(e);
        }
    })    
}
runInfo().then(console.log).catch(console.error);